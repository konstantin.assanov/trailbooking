FROM adoptopenjdk/openjdk11:alpine-jre
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} trail-booking.jar
ENTRYPOINT ["java", "-jar", "/trail-booking.jar"]