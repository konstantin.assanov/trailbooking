package com.exercise.trailbooking.services.impl;

import com.exercise.trailbooking.dto.BookingDto;
import com.exercise.trailbooking.dto.BookingRequestDto;
import com.exercise.trailbooking.dto.HikerDto;
import com.exercise.trailbooking.entities.Booking;
import com.exercise.trailbooking.entities.Trail;
import com.exercise.trailbooking.exceptions.BadRequestException;
import com.exercise.trailbooking.exceptions.ResourceNotFoundException;
import com.exercise.trailbooking.repository.IBookingRepository;
import com.exercise.trailbooking.repository.ITrailRepository;
import com.exercise.trailbooking.services.converters.BookingConverter;
import com.exercise.trailbooking.validation.AgeValidator;
import com.exercise.trailbooking.validation.impl.BasicValidator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class BookingServiceTest {

    private BookingService service;

    private IBookingRepository repo;
    private ITrailRepository trailRepo;

    private BasicValidator<BookingRequestDto> validator;
    private AgeValidator ageValidator;
    private BookingConverter converter;

    @BeforeEach
    void init() {
        repo = mock(IBookingRepository.class);
        trailRepo = mock(ITrailRepository.class);
        validator = mock(BasicValidator.class);
        ageValidator = mock(AgeValidator.class);
        converter = mock(BookingConverter.class);
        service = new BookingService(repo, trailRepo, validator, ageValidator, converter);
    }

    // find booking

    @Test
    void shouldReturnRightBookingFromRepository() {

        Booking booking = mock(Booking.class);
        BookingDto bookingDto = mock(BookingDto.class);

        when(repo.findById(132L)).thenReturn(Optional.of(booking));

        when(converter.toDto(booking)).thenReturn(bookingDto);

        BookingDto found = service.get(132L);

        assertNotNull(found);
        assertEquals(bookingDto, found);

    }

    @Test
    void shouldThrowResourceNotFoundExceptionOnFindIfNotFound() {

        when(repo.findById(132L)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () ->
                service.get(132L));
    }

    // create booking

    @Test
    void shouldCreateSuccessfully() {

        BookingRequestDto requestDto = mock(BookingRequestDto.class);
        when(validator.validate(requestDto)).thenReturn(requestDto);

        Trail trail = mock(Trail.class);
        when(requestDto.getTrailId()).thenReturn(111L);

        HikerDto[] hikers = new HikerDto[]{ mock(HikerDto.class) };
        when(requestDto.getHikers()).thenReturn(hikers);

        when(ageValidator.validate(trail, hikers)).thenReturn(trail);

        when(trailRepo.findById(111L)).thenReturn(Optional.of(trail));

        Booking created = mock(Booking.class);

        when(repo.save(any())).thenReturn(created);

        BookingDto createdDto = mock(BookingDto.class);

        when(converter.toDto(created)).thenReturn(createdDto);

        BookingDto resultDto = service.create(requestDto);

        assertEquals(createdDto, resultDto);

    }

    @Test
    void shouldFailOnCreateOnInvalidRequest() {
        BookingRequestDto requestDto = mock(BookingRequestDto.class);

        when(validator.validate(requestDto)).thenThrow(BadRequestException.class);

        assertThrows(BadRequestException.class, () ->
                service.create(requestDto));
    }

    @Test
    void shouldFailOnCreateIfUnknownTrail() {
        BookingRequestDto requestDto = mock(BookingRequestDto.class);

        when(validator.validate(requestDto)).thenReturn(requestDto);

        when(requestDto.getTrailId()).thenReturn(111L);
        when(trailRepo.findById(111L)).thenReturn(Optional.empty());

        assertThrows(BadRequestException.class, () ->
                service.create(requestDto));
    }

    @Test
    void shouldFailOnCreateIfInvalidHikerAge() {
        BookingRequestDto requestDto = mock(BookingRequestDto.class);

        when(validator.validate(requestDto)).thenReturn(requestDto);

        Trail trail = mock(Trail.class);
        HikerDto[] hikers = new HikerDto[]{ mock(HikerDto.class), mock(HikerDto.class) };

        when(requestDto.getHikers()).thenReturn(hikers);
        when(ageValidator.validate(trail, hikers)).thenThrow(BadRequestException.class);

        when(requestDto.getTrailId()).thenReturn(111L);
        when(trailRepo.findById(111L)).thenReturn(Optional.of(trail));

        assertThrows(BadRequestException.class, () ->
                service.create(requestDto));
    }

    // cancel booking

    @Test
    void shouldExecuteRightBookingDeletionFromRepository() {

        Booking booking = mock(Booking.class);

        when(repo.findById(132L)).thenReturn(Optional.of(booking));

        service.cancel(132L);

        verify(repo).delete(booking);
    }

    @Test
    void shouldThrowResourceNotFoundExceptionOnCancelIfNotFound() {

        when(repo.findById(132L)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () ->
                service.cancel(132L));
    }

}