package com.exercise.trailbooking.services.impl;

import com.exercise.trailbooking.dto.TrailDto;
import com.exercise.trailbooking.entities.Trail;
import com.exercise.trailbooking.exceptions.ResourceNotFoundException;
import com.exercise.trailbooking.repository.ITrailRepository;
import com.exercise.trailbooking.services.converters.TrailConverter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TrailServiceTest {

    TrailService service;

    ITrailRepository repo;

    TrailConverter converter;

    @BeforeEach
    void init() {
        repo = mock(ITrailRepository.class);
        converter = mock(TrailConverter.class);
        service = new TrailService(repo, converter);
    }

    @Test
    void shouldReturnAllTrailsFromRepository() {

        List<Trail> trails = List.of(mock(Trail.class), mock(Trail.class));
        List<TrailDto> trailDtos = List.of(mock(TrailDto.class), mock(TrailDto.class));

        when(repo.findAll()).thenReturn(trails);

        when(converter.toDto(trails.get(0))).thenReturn(trailDtos.get(0));
        when(converter.toDto(trails.get(1))).thenReturn(trailDtos.get(1));

        List<TrailDto> list = service.trails();

        assertEquals(2, list.size());

        assertEquals(trailDtos.get(0), list.get(0));
        assertEquals(trailDtos.get(1), list.get(1));

    }

    @Test
    void shouldReturnRightTrailFromRepository() {

        Trail trail = mock(Trail.class);
        TrailDto trailDto = mock(TrailDto.class);

        when(repo.findById(132L)).thenReturn(Optional.of(trail));

        when(converter.toDto(trail)).thenReturn(trailDto);

        TrailDto found = service.trail(132L);

        assertNotNull(found);
        assertEquals(trailDto, found);

    }

    @Test
    void shouldThrowResourceNotFoundExceptionIfNotFound() {

        when(repo.findById(132L)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () ->
            service.trail(132L));
    }

}