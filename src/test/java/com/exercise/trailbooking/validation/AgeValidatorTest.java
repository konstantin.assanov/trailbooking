package com.exercise.trailbooking.validation;

import com.exercise.trailbooking.dto.HikerDto;
import com.exercise.trailbooking.entities.Trail;
import com.exercise.trailbooking.exceptions.BadRequestException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class AgeValidatorTest {

    private AgeValidator ageValidator;

    @BeforeEach
    void init() {
        ageValidator = new AgeValidator();
    }

    @Test
    void shouldValidateSuccessfully() {

        Trail trail = mock(Trail.class);
        when(trail.getMinimumAge()).thenReturn(10);
        when(trail.getMaximumAge()).thenReturn(25);

        HikerDto[] hikers = new HikerDto[]{
            new HikerDto("Hiker#1", 14),
            new HikerDto("Hiker#2", 25)
        };

        Trail validated = ageValidator.validate(trail, hikers);

        assertEquals(trail, validated);
    }

    @Test
    void shouldThrowExceptionIfTooYoung() {

        Trail trail = mock(Trail.class);
        when(trail.getMinimumAge()).thenReturn(10);
        when(trail.getMaximumAge()).thenReturn(25);

        HikerDto[] hikers = new HikerDto[]{
            new HikerDto("Hiker#1", 9),
            new HikerDto("Hiker#2", 25)
        };

        assertThrows(BadRequestException.class, () ->
                ageValidator.validate(trail, hikers));
    }

    @Test
    void shouldThrowExceptionIfTooOld() {

        Trail trail = mock(Trail.class);
        when(trail.getMinimumAge()).thenReturn(10);
        when(trail.getMaximumAge()).thenReturn(25);

        HikerDto[] hikers = new HikerDto[]{
                new HikerDto("Hiker#1", 12),
                new HikerDto("Hiker#2", 30)
        };

        assertThrows(BadRequestException.class, () ->
                ageValidator.validate(trail, hikers));
    }

}