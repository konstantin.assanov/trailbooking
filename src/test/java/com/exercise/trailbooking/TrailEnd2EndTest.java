package com.exercise.trailbooking;

import com.exercise.trailbooking.dto.TrailDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class TrailEnd2EndTest {

    @Autowired
    private MockMvc mockMvc;

    private ObjectMapper objectMapper = new ObjectMapper();

    private TrailDto[] dtoArrFromJson(String json) {
        try {
            return objectMapper.readValue(json, TrailDto[].class);
        }
        catch (Exception e) {
            System.out.println("Failed: " + json + " e=> " + e);
            return new TrailDto[]{};
        }
    }

    @Test
    void shouldReturnCorrectListOfTrails() throws Exception {

        String result = mockMvc.perform(get("/trails")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        TrailDto[] dtos = dtoArrFromJson(result);

        assertEquals(3, dtos.length);

        assertTrue(Arrays.asList(dtos).contains(new TrailDto(
                1, "Shire",
                "07:00", "09:00",
                5, 100,
                29.90)));

    }

    @Test
    void shouldExtractCorrectTrailById() throws Exception {

        mockMvc.perform(get("/trails/1")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.name", is("Shire")))
                .andExpect(jsonPath("$.startAt", is("07:00")))
                .andExpect(jsonPath("$.endAt", is("09:00")))
                .andExpect(jsonPath("$.minimumAge", is(5)))
                .andExpect(jsonPath("$.maximumAge", is(100)))
                .andExpect(jsonPath("$.unitPrice", is(29.90)));

    }

    @Test
    void shouldFailWhenGetTrailOnInvalidTrailIdWithNotFound404() throws Exception {

        mockMvc.perform(get("/trails/10")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());

    }

}