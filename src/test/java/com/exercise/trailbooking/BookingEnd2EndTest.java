package com.exercise.trailbooking;

import com.exercise.trailbooking.dto.BookingRequestDto;
import com.exercise.trailbooking.dto.HikerDto;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.jayway.jsonpath.JsonPath;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class BookingEnd2EndTest {

    @Autowired
    private MockMvc mockMvc;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Test
    void shouldCreateViewAndCancelBookingWithSuccess() throws Exception {

        HikerDto[] hikers = new HikerDto[] {
                new HikerDto("Hiker#1", 32),
                new HikerDto("Hiker#2", 37)
        };
        BookingRequestDto requestDto = new BookingRequestDto(1, hikers, "10.12.2021");

        // booking must be successful
        String result = mockMvc.perform(post("/bookings")
                .content(objectMapper.writeValueAsString(requestDto))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.trailId", is((int)requestDto.getTrailId())))
                .andExpect(jsonPath("$.hikers[0].name", is(requestDto.getHikers()[0].getName())))
                .andExpect(jsonPath("$.hikers[0].age", is(requestDto.getHikers()[0].getAge())))
                .andExpect(jsonPath("$.hikers[1].name", is(requestDto.getHikers()[1].getName())))
                .andExpect(jsonPath("$.hikers[1].age", is(requestDto.getHikers()[1].getAge())))
                .andExpect(jsonPath("$.date", is(requestDto.getDate())))
                .andReturn()
                .getResponse()
                .getContentAsString();

        int bookingId = JsonPath.read(result, "$.id");

        // booking can be extracted
        mockMvc.perform(get("/bookings/" + bookingId)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.trailId", is((int)requestDto.getTrailId())))
                .andExpect(jsonPath("$.hikers[0].name", is(requestDto.getHikers()[0].getName())))
                .andExpect(jsonPath("$.hikers[0].age", is(requestDto.getHikers()[0].getAge())))
                .andExpect(jsonPath("$.hikers[1].name", is(requestDto.getHikers()[1].getName())))
                .andExpect(jsonPath("$.hikers[1].age", is(requestDto.getHikers()[1].getAge())))
                .andExpect(jsonPath("$.date", is(requestDto.getDate())));

        // remove booking by id
        mockMvc.perform(delete("/bookings/" + bookingId)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

        // removed booking can't be found
        mockMvc.perform(get("/bookings/" + bookingId)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());

    }

    @Test
    void shouldFailWhenBookWithInvalidBookingRequestWithBadRequest400() throws Exception {
        BookingRequestDto requestDto = new BookingRequestDto(1, null, "10.12.2021");

        mockMvc.perform(post("/bookings")
                .content(objectMapper.writeValueAsString(requestDto))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());

    }

    @Test
    void shouldFailWhenBookWithBookingRequestForTooOldHikerWithBadRequest400() throws Exception {
        BookingRequestDto requestDto = new BookingRequestDto(
                1,
                new HikerDto[]{
                        new HikerDto("Hiker#1", 54),
                        new HikerDto("Hiker#2", 101)},
                "10.12.2021");

        mockMvc.perform(post("/bookings")
                .content(objectMapper.writeValueAsString(requestDto))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());

    }

    @Test
    void shouldFailWhenGetBookingOnInvalidBookingIdWithNotFound404() throws Exception {

        mockMvc.perform(get("/bookings/101")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());

    }

    @Test
    void shouldFailWhenCancelBookingOnInvalidBookingIdWithNotFound404() throws Exception {

        mockMvc.perform(delete("/bookings/101")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());

    }

}