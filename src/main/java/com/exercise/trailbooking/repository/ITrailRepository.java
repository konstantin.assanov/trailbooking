package com.exercise.trailbooking.repository;

import com.exercise.trailbooking.entities.Trail;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ITrailRepository extends CrudRepository<Trail, Long> {
}