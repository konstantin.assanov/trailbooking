package com.exercise.trailbooking.repository;

import com.exercise.trailbooking.entities.Booking;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IBookingRepository extends CrudRepository<Booking, Long> {
}