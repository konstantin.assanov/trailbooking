package com.exercise.trailbooking.services;

import com.exercise.trailbooking.dto.TrailDto;

import java.util.List;

public interface ITrailService {

    List<TrailDto> trails();

    TrailDto trail(long trailId);

}