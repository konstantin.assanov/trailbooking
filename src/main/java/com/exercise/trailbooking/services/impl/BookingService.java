package com.exercise.trailbooking.services.impl;

import com.exercise.trailbooking.dto.BookingDto;
import com.exercise.trailbooking.dto.BookingRequestDto;
import com.exercise.trailbooking.entities.Booking;
import com.exercise.trailbooking.entities.Trail;
import com.exercise.trailbooking.exceptions.BadRequestException;
import com.exercise.trailbooking.exceptions.ResourceNotFoundException;
import com.exercise.trailbooking.services.IBookingService;
import com.exercise.trailbooking.services.converters.BookingConverter;
import com.exercise.trailbooking.repository.IBookingRepository;
import com.exercise.trailbooking.repository.ITrailRepository;
import com.exercise.trailbooking.validation.AgeValidator;
import com.exercise.trailbooking.validation.IBasicValidator;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class BookingService implements IBookingService {

    private final IBookingRepository repo;
    private final ITrailRepository trailRepo;
    private final IBasicValidator<BookingRequestDto> dtoValidator;
    private final AgeValidator ageValidator;
    private final BookingConverter converter;

    BookingService(IBookingRepository repo,
                   ITrailRepository trailRepo,
                   IBasicValidator<BookingRequestDto> dtoValidator,
                   AgeValidator ageValidator,
                   BookingConverter converter) {
        this.repo = repo;
        this.trailRepo = trailRepo;
        this.dtoValidator = dtoValidator;
        this.ageValidator = ageValidator;
        this.converter = converter;
    }

    @Override
    public BookingDto get(long id) {
        return repo
                .findById(id)
                .map(converter::toDto)
                .orElseThrow(() -> new ResourceNotFoundException("Invalid Booking ID: " + id));
    }

    @Override
    @Transactional
    public BookingDto create(BookingRequestDto dto) {
        return Optional.of(dto)
                .map(dtoValidator::validate)
                .map(BookingRequestDto::getTrailId)
                .flatMap(trailRepo::findById)
                .map(trail -> ageValidator.validate(trail, dto.getHikers()))
                .map(trail -> newBooking(trail, dto))
                .map(repo::save)
                .map(converter::toDto)
                .orElseThrow(() -> new BadRequestException(""));
    }

    private Booking newBooking(Trail trail, BookingRequestDto dto) {
        return new Booking(0,
                trail,
                dto.getHikers(),
                dto.getDate());
    }

    @Override
    @Transactional
    public void cancel(long id) {
        repo.findById(id)
            .ifPresentOrElse(
                    repo::delete,
                    () -> {
                        throw new ResourceNotFoundException("Invalid Booking ID: " + id);
                    });
    }

}