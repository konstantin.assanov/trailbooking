package com.exercise.trailbooking.services.impl;

import com.exercise.trailbooking.dto.TrailDto;
import com.exercise.trailbooking.exceptions.ResourceNotFoundException;
import com.exercise.trailbooking.services.ITrailService;
import com.exercise.trailbooking.services.converters.TrailConverter;
import com.exercise.trailbooking.repository.ITrailRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class TrailService implements ITrailService {

    private final ITrailRepository repo;
    private final TrailConverter converter;

    TrailService(ITrailRepository repo, TrailConverter converter) {
        this.repo = repo;
        this.converter = converter;
    }

    @Override
    public List<TrailDto> trails() {
        return StreamSupport
                .stream(repo.findAll().spliterator(), false)
                .map(converter::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public TrailDto trail(long trailId) {
        return repo
                .findById(trailId)
                .map(converter::toDto)
                .orElseThrow(() -> new ResourceNotFoundException("Invalid Trail ID: " + trailId));
    }

}