package com.exercise.trailbooking.services;

import com.exercise.trailbooking.dto.BookingDto;
import com.exercise.trailbooking.dto.BookingRequestDto;

public interface IBookingService {

    BookingDto get(long id);

    BookingDto create(BookingRequestDto dto);

    void cancel(long id);

}