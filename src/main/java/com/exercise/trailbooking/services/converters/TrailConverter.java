package com.exercise.trailbooking.services.converters;

import com.exercise.trailbooking.dto.TrailDto;
import com.exercise.trailbooking.entities.Trail;
import org.springframework.stereotype.Service;

@Service
public class TrailConverter {

    public TrailDto toDto(Trail trail) {
        return new TrailDto(trail.getId(),
                trail.getName(),
                trail.getStartAt(),
                trail.getEndAt(),
                trail.getMinimumAge(),
                trail.getMaximumAge(),
                trail.getUnitPrice());
    }

}
