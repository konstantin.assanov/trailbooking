package com.exercise.trailbooking.services.converters;

import com.exercise.trailbooking.dto.HikerDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class HikersConverter implements AttributeConverter<HikerDto[], String> {

    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public String convertToDatabaseColumn(HikerDto[] hikers) {
        try {
            return objectMapper.writeValueAsString(hikers);
        } catch (JsonProcessingException jpe) {
            return null;
        }
    }

    @Override
    public HikerDto[] convertToEntityAttribute(String strHikers) {
        try {
            return objectMapper.readValue(strHikers, HikerDto[].class);
        } catch (JsonProcessingException jpe) {
            return null;
        }
    }

}