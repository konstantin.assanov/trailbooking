package com.exercise.trailbooking.services.converters;

import com.exercise.trailbooking.dto.BookingDto;
import com.exercise.trailbooking.entities.Booking;
import org.springframework.stereotype.Service;

@Service
public class BookingConverter {

    public BookingDto toDto(Booking booking) {
        return new BookingDto(booking.getId(),
                booking.getTrail().getId(),
                booking.getHikers(),
                booking.getDate());
    }

}