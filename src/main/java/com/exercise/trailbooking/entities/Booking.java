package com.exercise.trailbooking.entities;

import com.exercise.trailbooking.dto.HikerDto;
import com.exercise.trailbooking.services.converters.HikersConverter;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@Entity
@Table(name = "bookings")
public class Booking {

    @Version
    private long version;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TRAIL_ID")
    private Trail trail;

    @NotEmpty
    @Column
    @Convert(converter = HikersConverter.class)
    private HikerDto[] hikers;

    @Size(min = 10, max = 10)
    @Column(name = "_date")
    private String date;

    public Booking(long id,
                   Trail trail,
                   HikerDto[] hikers,
                   String date) {
        this.id = id;
        this.trail = trail;
        this.hikers = hikers;
        this.date = date;
    }

}