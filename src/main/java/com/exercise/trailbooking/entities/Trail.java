package com.exercise.trailbooking.entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;

@Data
@NoArgsConstructor
@Entity
@Table(name = "trails")
public class Trail {

    @Version
    private long version;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotBlank
    @Column(unique = true)
    private String name;

    @Pattern(regexp = "^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$")
    private String startAt;

    @Pattern(regexp = "^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$")
    private String endAt;

    @Positive(message = "Minimum Hiker Age must be positive number")
    private int minimumAge;

    @Positive(message = "Maximum Hiker Age must be positive number")
    private int maximumAge;

    @PositiveOrZero(message = "Unit Price must be positive number or zero")
    private double unitPrice;

    public Trail(long id,
                 String name,
                 String startAt,
                 String endAt,
                 int minimumAge,
                 int maximumAge,
                 double unitPrice) {
        this.id = id;
        this.name = name;
        this.startAt = startAt;
        this.endAt = endAt;
        this.minimumAge = minimumAge;
        this.maximumAge = maximumAge;
        this.unitPrice = unitPrice;
    }

}