package com.exercise.trailbooking.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@NoArgsConstructor(force=true)
@AllArgsConstructor
@Setter(AccessLevel.NONE)
public class TrailDto {

    private final long id;

    private final String name;

    private final String startAt;

    private final String endAt;

    private final int minimumAge;

    private final int maximumAge;

    private final double unitPrice;

}