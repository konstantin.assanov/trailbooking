package com.exercise.trailbooking.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor(force=true)
@AllArgsConstructor
@Setter(AccessLevel.NONE)
public class BookingRequestDto {

    @NotNull(message = "Trail ID cannot be null")
    @Positive(message = "Trail ID must be positive number")
    private final long trailId;

    @NotEmpty(message = "At least 1 Hiker must be defined")
    private final HikerDto[] hikers;

    @Size(min = 10, max = 10)
    private final String date;

}