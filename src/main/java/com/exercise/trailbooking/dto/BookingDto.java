package com.exercise.trailbooking.dto;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Setter(AccessLevel.NONE)
public class BookingDto extends BookingRequestDto {

    private final long id;

    public BookingDto(long id,
               long trailId,
               HikerDto[] hikers,
               String date) {
        super(trailId, hikers, date);
        this.id = id;
    }

}
