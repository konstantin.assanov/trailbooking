package com.exercise.trailbooking.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
@NoArgsConstructor(force=true)
@AllArgsConstructor
@Setter(AccessLevel.NONE)
public class HikerDto implements Serializable {

    @NotBlank(message = "Hiker Name cannot be null or blank")
    @Size(min = 2, max = 128, message = "Hiker Name must be between 2 and 128 characters")
    private final String name;

    @NotNull(message = "Hiker Age cannot be null")
    @Positive(message = "Hiker Age must be positive number")
    private final int age;

}
