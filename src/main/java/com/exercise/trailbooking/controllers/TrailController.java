package com.exercise.trailbooking.controllers;

import com.exercise.trailbooking.dto.TrailDto;
import com.exercise.trailbooking.services.ITrailService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/trails")
public class TrailController {

    private final ITrailService trailService;

    TrailController(ITrailService service) {
        this.trailService = service;
    }

    @GetMapping("")
    List<TrailDto> trails() {
        return trailService.trails();
    }

    @GetMapping("/{id}")
    TrailDto trail(@PathVariable("id") long id) {
        return trailService.trail(id);
    }

}
