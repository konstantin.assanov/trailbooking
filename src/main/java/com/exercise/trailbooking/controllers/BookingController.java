package com.exercise.trailbooking.controllers;

import com.exercise.trailbooking.dto.BookingDto;
import com.exercise.trailbooking.dto.BookingRequestDto;
import com.exercise.trailbooking.services.IBookingService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/bookings")
public class BookingController {

    private final IBookingService bookingService;

    BookingController(IBookingService service) {
        this.bookingService = service;
    }

    @GetMapping("/{id}")
    public BookingDto booking(@PathVariable("id") long id) {
        return bookingService.get(id);
    }

    @PostMapping()
    public BookingDto book(@RequestBody BookingRequestDto dto) {
        return bookingService.create(dto);
    }

    @DeleteMapping("/{id}")
    public void cancel(@PathVariable("id") long id) {
        bookingService.cancel(id);
    }

}