package com.exercise.trailbooking.validation;

import com.exercise.trailbooking.dto.HikerDto;
import com.exercise.trailbooking.entities.Trail;
import com.exercise.trailbooking.exceptions.BadRequestException;
import org.springframework.stereotype.Service;

import java.util.stream.Stream;

import static java.util.stream.Collectors.joining;

@Service
public class AgeValidator {

    public Trail validate(Trail trail, HikerDto[] hikers) {
        String outOfAge = Stream.of(hikers)
                .filter(hiker -> (hiker.getAge() < trail.getMinimumAge())
                        || (hiker.getAge() > trail.getMaximumAge()))
                .map(HikerDto::getName)
                .collect(joining(","));

        if (!outOfAge.isBlank()) {
            throw new BadRequestException("Following hikers are not admitted by age: " + outOfAge);
        }

        return trail;
    }

}