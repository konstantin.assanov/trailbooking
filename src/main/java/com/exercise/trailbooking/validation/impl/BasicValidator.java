package com.exercise.trailbooking.validation.impl;

import com.exercise.trailbooking.exceptions.BadRequestException;
import com.exercise.trailbooking.validation.IBasicValidator;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BasicValidator<T> implements IBasicValidator<T> {

    private final Validator validator;

    BasicValidator(Validator validator) {
        this.validator = validator;
    }

    private List<String> _validate(T data) {
        return validator
                .validate(data)
                .stream()
                .map(ConstraintViolation::getMessage)
                .collect(Collectors.toList());
    }

    @Override
    public T validate(T data) throws BadRequestException {
        final List<String> errors = _validate(data);
        if (!errors.isEmpty()) {
            throw new BadRequestException(errors);
        }

        return data;
    }

}