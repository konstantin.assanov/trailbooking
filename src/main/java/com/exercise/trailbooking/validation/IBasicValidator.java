package com.exercise.trailbooking.validation;

public interface IBasicValidator<T> {

    T validate(T order);

}
