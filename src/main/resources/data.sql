INSERT INTO trails (id, version, name, start_at, end_at, minimum_Age, maximum_Age, unit_Price)
VALUES (1, 0, 'Shire', '07:00', '09:00', 5, 100, 29.90);

INSERT INTO trails (id, version, name, start_at, end_at, minimum_Age, maximum_Age, unit_Price)
VALUES (2, 0, 'Gondor', '10:00', '13:00', 11, 50, 59.90);

INSERT INTO trails (id, version, name, start_at, end_at, minimum_Age, maximum_Age, unit_Price)
VALUES (3, 0, 'Mordor', '14:00', '19:00', 18, 40, 99.90);