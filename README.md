# Trail Booking

## 1. Requests Overview

* View all the trails available for hiking
* View a selected trail
* Book a selected trail for hiking
* View a booking
* Cancel a booking

## 2. Implementation tools

* Java 11
* Spring Boot
* Lombok
* JUnit
* Mockito
* MockMvc
* PostgreSql
* Maven

## 3. Assumptions & Simplifications

**1.** Date & Time has not impact on booking in the given scenario.

That's why the date & time are treated as strings:
* 'startAt' & 'endAt' hours of a trail are processed and stored as strings; 'hh:mmm' format is however required;
* 'date' field of a booking is processed and stored as a string. <br>No format is imposed.<br> Also there is no check that the
    booking date is indeed in the future

**2.** No Hikers identity control

In a real-life situation Hiker should be processed with their own Domain model, and stored in a separate 'Hiker' table;<br>

The unique id (primary key of 'Hiker' table) could be used to identify a Hiker instead of passing the Hikers by name
for booking. Consequently, the doubles of hikers would be eliminated; the possible conflictual definitions
of their ages would be detected.

Also, it would be easier to manage the elimination of doubles of Hikers in the same Booking request.<br>
In the current version, it's possible to put more than one Hiker with the same name for the same Booking.<br>

**2.1.** Current implementation only checks that all mentioned hiker ages for the given trail are satisfactory.<br>

**2.2.** Current implementation stores the list of Hikers' names and ages, submitted for a booking, as a json-string
in a single column.<br>

**2.3.** Current implementation accepts a list of hikers for a single booking without a distinction who would be a principle
hiker, making the reservation. It would be better to distinguish 1 single hiker who is responsible for the
booking and the others for whom he/she does a reservation.

**3.** Additional Unit Tests can be added [Cf.](#unit-tests-which-could-be-added)

**4.** Entire trail or none

It's assumed that the booking is done for the entire period of a Trail. It's not possible to join the trail
later than beginning, or finish earlier than the end of trail.

**5.** All trails view

Currently the whole list of all available trails is returned at once. If a longer list, it would better to introduce paging.

**6.** DB availability

Database can become unavailable. In this case the connection exception has to be caught explicitly and
an appropriate message to the front-end should be returned.<br>
Current implementation does not treat it explicitly.

**7.** @Version

@Version field is added into each Entity definition for optimistic locking.<br>
Optimistic locking is not necessary needed for the given scenario, because there is no update requests, but I left it
if once needed.

## 4. To Better Domains model

### 4.1. Model

1. **Hikers** table

    Contains the unique entry for each Hiker

2. **Hiker-Schedule** Many-to-Many table

    Unique on **(scheduleId, hikerId)** pair.<br>

    Because the Act of Booking can be common to a group of hikers,
    an additional table **GroupBooking** can be defined.<br>
    It would contain an additional information related to group booking; for example, number of
    hikers in the group, average age etc.<br>

    Each entry at **Hiker-Schedule** would contain a foreign key reference to the entry in **GroupBooking** table.<br>

3. **Schedule** table

    Unique on **(trailId, Date)** pair.<br>

    Collects information on actual bookings. For example, how many hikers have booked
    for the given Trail on the given Date. <br>
    Consequently, it would be possible to calculate the planed income.

    *update-process option:*

    In real-life scenario the available slots (trailId, Date) may be programmed/filled in advance
    with 0 participants, and then, when a booking request, the update to
    the 'Schedule' table is done.

4. **Trails** table

    Same as defined.

### 4.2. Advantages

1. Booking race condition

    'Schedule' table would contain, for example, a number of available places. It will be easier
    to manage the race condition for reservations.

2. Schedule analyze

    Schedule table would help to analyze if there is an overlap in time between the scheduled events.<br>
    if there is a time overlap, it's possible to control that a Hiker is not booking for the
    overlapping events.

### 4.3. Picture

![title](BetterModel.jpeg)

## 5. Tests

### 5.1. End-To-End Tests

**1. Trail requests tests:**

* successful list of trails<br>
* successful view of a trail by Id<br>
* fail view of a train by Invalid Id<br>

**2. Booking requests tests:**

* successful scenario: new booking - get booking - cancel booking <br>
* fail to book with an invalid request
* fail to book with a hiker that is too old
* fail to view non-existent booking
* fail to cancel non-existent booking

### 5.2. Unit Tests

**1. Age Validator tests:**

* successful validation for the right ages
* invalid if hiker is too young
* invalid if hiker is too old

**2. Trail Service tests:**

* successful list all trails defined in repository
* successful view of a trail by id if defined in repostiory
* fail to view trail if not defined in repository

**3. Booking Service tests:**

* successful get booking from repository
* fail to get booking if not defined in repository
* successful creation of a booking if request data is valid
* fail to create a booking if invalid request
* fail to create a booking if unknown trail
* fail to create a booking if invalid hiker age
* successful to cancel a booking if defined in repository
* fail to cancel a booking if not defined in repository

### 5.3. Unit Tests which could be added:

* 'dto' validation unit tests
* 'entities' validation unit tests
* 'services.converters' unit tests

## 6. Profiles

**1. Dev**: 'create-drop' on H2

**2. Test**: 'create-drop' on a pre-defined local Postgresql DB ('trails_test')

**3. Prod**: 'validate' on a Postgresql DB ('trails') which is defined with the environment: **DB_SERV**, **DB_USR**, **DB_PWD**

## 7. Build

    mvn clean package

## 8. Run Tests

*H2: 'dev' profile:*

    mvn test

*Local Postgresql DB: 'test' profile:*

    mvn -Dspring.profiles.active=test test

*Postgresql DB: 'prod' profile:*

    mvn -Dspring.profiles.active=prod -DDB_SERV=localhost:5432 -DDB_USR=postgres -DDB_PWD=admin test

## 9. Run

*H2: 'dev' profile:*

    java -jar target/trail-booking-0.0.1-SNAPSHOT.jar

*Local Postgresql DB: 'test' profile:*

    java -jar -Dspring.profiles.active=test target/trail-booking-0.0.1-SNAPSHOT.jar

*Postgresql DB: 'prod' profile:*

    java -jar -Dspring.profiles.active=prod -DDB_SERV=localhost:5432 -DDB_USR=postgres -DDB_PWD=admin target/trail-booking-0.0.1-SNAPSHOT.jar

## 10. Docker deployment

H2:

     docker build -t trailbooking .
     docker run -p 8080:8080 trailbooking

PostgreSql:

    docker-compose up
    docker-compouse down
